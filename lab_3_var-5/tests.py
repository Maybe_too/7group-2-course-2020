import test
from solve import is_palindrome

test.assert_equals(is_palindrome('abacba'), True)
test.assert_equals(is_palindrome('abacaba'), True)
test.assert_equals(is_palindrome('abaaba'), True)
test.assert_equals(is_palindrome(''), True)
test.assert_equals(is_palindrome('a'), True)
test.assert_equals(is_palindrome('xxxxx'), True)
test.assert_equals(is_palindrome('zzzz'), True)
test.assert_equals(is_palindrome('ztzz'), False)
test.assert_equals(is_palindrome('ztz'), True)
