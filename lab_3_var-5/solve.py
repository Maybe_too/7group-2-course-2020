def is_palindrome(s):
    if (len(s) <= 1):
        return True
    else:
        if s[0] != s[-1]:
            return False
        else:
            return is_palindrome(s[1:-1])
