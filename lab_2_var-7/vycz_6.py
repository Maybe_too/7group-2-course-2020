def circular_shift(lst1, lst2, n):
    n1 = len(lst1)
    n2 = len(lst2)
    if (n1 != n2):
        return False
      
    for i in range(n1):
        j = (i + n) % n1
        if (j < 0):
            j += n1
            
        if (lst1[j] != lst2[i]):
            return False
    
    return True
    
print(circular_shift([1, 2, 3, 4], [3, 4, 1, 2], 2))
print(circular_shift([1, 1], [1, 1], 6))
print(circular_shift([0, 1, 2, 3, 4, 5], [3, 4, 5, 2, 1, 0], 3))
print(circular_shift([0, 1, 2, 3], [1, 2, 3, 1], 1))
print(circular_shift(list(range(32)), list(range(32)), 0))
print(circular_shift([1, 2, 1], [1, 2, 1], 3))
print(circular_shift([5, 7, 2, 3], [2, 3, 5, 7], -2))