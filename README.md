### Петровский Константин. 2 курс, 7-я группа

### Индивидуальное задание №1 [вариант 6](https://bitbucket.org/Maybe_too/7group-2-course-2020/src/master/lab_1_var-6/)
### Индивидуальное задание №2 [вариант 7](https://bitbucket.org/Maybe_too/7group-2-course-2020/src/master/lab_2_var-7/)
### Индивидуальное задание №3 [вариант 5](https://bitbucket.org/Maybe_too/7group-2-course-2020/src/master/lab_3_var-5/)
### Cайт с визуализацией [online](https://hotelinvaleva.herokuapp.com/)
### [notoptimized](https://bitbucket.org/Maybe_too/7group-2-course-2020/src/master/not_optimized/)
### [optimized](https://bitbucket.org/Maybe_too/7group-2-course-2020/src/master/optimized/)
### [site with vizualization](https://bitbucket.org/Maybe_too/7group-2-course-2020/src/master/site%20with%20vizualization/)