def is_linear(s):
    #s(n) = a * n + b
    #s[0] = a * 0 + b = b
    #s[1] = a * 1 + b = a + b
    b = s[0]
    a = s[1] - b
    
    for i in range(len(s)):
        if (b + a * i != s[i]):
            return False
    
    return True
    
def is_quadratic(s):
    #s(n) = a * n * n + b * n + c
    #s[0] = a * 0 * 0 + b * 0 + c = c
    #s[1] = a + b + c
    #s[2] = a * 4 + b * 2 + c
    c = s[0]
    a = (c - 2 * s[1] + s[2]) * 0.5
    b = (-3 * c + 4 * s[1] - s[2]) * 0.5
    
    for i in range(len(s)):
        if (abs(a * i * i + b * i + c - s[i]) > 1e-7):
            return False
    
    return True
    
def is_cubic(s):
    #s(n) = a * n * n * n + b * n * n + c * n + d
    #s[0] = d
    #s[1] = a + b + c + d
    #s[2] = a * 8 + b * 4 + c * 2 + d
    #s[3] = a * 27 + b * 9 + c * 3 + d
    d = s[0]
    a = (-d + 3 * s[1] - 3 * s[2] + s[3]) / 6.0
    b = (2 * d - 5 * s[1] + 4 * s[2] - s[3]) * 0.5
    c = (-11 * d + 18 * s[1] - 9 * s[2] + 2 * s[3]) / 6.0
    
    for i in range(len(s)):
        if (abs(a * i * i * i + b * i * i + c * i + d - s[i]) > 1e-7):
            return False
    
    return True

def seq_level(seq):
    if (is_linear(seq)):
        return "Linear"
    elif (is_quadratic(seq)):
        return "Quadratic"
    elif (is_cubic(seq)):
        return "Cubic"
    else:
        return "Undefined"
    
print(seq_level([1, 2, 3, 4, 5]))
print(seq_level([2, 1, 0, -1, -2]))
print(seq_level([3, 6, 10, 15, 21]))
print(seq_level([4, 9, 16, 25, 36]))
print(seq_level([7, 17, 31, 49, 71]))
print(seq_level([2, 10, 26, 50, 82]))
print(seq_level([-6, -4, 10, 42, 98, 184]))
print(seq_level([17, 59, 143, 287, 509, 827]))